namespace MyTestApi.Tests;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using System.Net;
public class UnitTest1
{
    // Scenario : Le pays possède qu’une seule capitale
    // GIVEN : « France »
    // WHEN je demande la capitale
    // THEN je récupère le string contenant {« Paris »}

    //Tester un pays présent dans la base
    [Fact]
    public async Task IsGetPaysCapitalbyPays()
    {
        await using var _factory = new WebApplicationFactory<Program>();
        var client = _factory.CreateClient();

        var response = await client.GetAsync("CapitalPays/France");
        string StringResponse = await response.Content.ReadAsStringAsync();
        // Assert
        response.EnsureSuccessStatusCode();
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);

    }
    //Scenario : Cas KO le pays n’existe pas
    //GIVEN « Italie »
    //WHEN je demande une capitale
    //THEN je le code erreur et le string : « [404] - Pays introuvable ».

    //Tester un Pays non existant dans la base
    [Fact]
    public async Task IsGetPaysCapitalbyPaysError()
    {
        await using var _factory = new WebApplicationFactory<Program>();
        var client = _factory.CreateClient();

        var response = await client.GetAsync("CapitalPays/Italie");
        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

    }

    //Récupérer tous les pays de la base
    [Fact]
    public async Task IsGetPaysCapital()
    {
        await using var _factory = new WebApplicationFactory<Program>();
        var client = _factory.CreateClient();

        var response = await client.GetAsync("CapitalPays");
        string StringResponse = await response.Content.ReadAsStringAsync();
        // Assert
        response.EnsureSuccessStatusCode();
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);

    }
}