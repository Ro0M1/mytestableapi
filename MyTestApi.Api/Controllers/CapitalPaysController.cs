using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using System;
using System.Text.Json;
namespace MyTestApi.Api.Controllers;
[ApiController]
[Route("[controller]")]
public class CapitalPaysController : ControllerBase
{
    public class Pays
    {
        public string Nom { get; set; }
        public string Capitale { get; set; }
    }
    private List<Pays> ListPays = new List<Pays>
    {
        new Pays{Nom="France",Capitale="Paris"},
        new Pays{Nom="Allemagne",Capitale="Berlin"},
        new Pays{Nom="Espagne",Capitale="Madrid"}
    };
    [HttpGet(Name = "GetPays")]
    public ActionResult<List<Pays>> GetPays()
    {
        return Ok(ListPays);
    }
    [HttpGet("{pays}", Name = "GetCapitalByPays")]
    public ActionResult GetCapital(string pays)
    {
        for (int i = 0; i < ListPays.Count; i++)
        {
            if (ListPays[i].Nom == pays)
            {
                return Ok(ListPays[i].Capitale);
            }
        };
        return NotFound("[404] - Pays introuvable");
    }
}